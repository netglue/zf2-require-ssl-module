<?php
/**
 * Factory to return a URI Resolver instance
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2013 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 */

namespace NetglueSSL\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use NetglueSSL\Service\UriResolver;

class UriResolverFactory implements FactoryInterface {
	
	/**
	 * Create Options instance from config
	 * @param ServiceLocatorInterface $serviceLocator
	 * @return Options
	 */
	public function createService(ServiceLocatorInterface $serviceLocator) {
		$config = $serviceLocator->get('NetglueSSL\Service\Options');
		return new UriResolver($config);
	}
	
}