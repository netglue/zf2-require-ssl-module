<?php

namespace NetglueSSL\Mvc\Controller\Plugin;

use NetglueSSL\Service\UriResolver;

use Zend\Mvc\Controller\Plugin\Redirect;

class ForceSsl extends Redirect {
	
	/**
	 * UriResolver
	 * @var UriResolver
	 */
	protected $resolver;
	
	/**
	 * If the current request is not secured over SSL, redirect to the equivalent request for the SSL host configured
	 * @return void|Zend\Http\Response
	 */
	public function __invoke() {
		// Don't do anything if we're disabled...
		if(!$this->isEnabled()) {
			return;
		}
		
		$controller = $this->getController();
		$request = $controller->getRequest();
		
		/**
		 * If we're already connected with SSL, return
		 */
		if($this->getResolver()->isSSL($request)) {
			return;
		}
		/**
		 * Otherwise, redirect to the resolved SSL Uri
		 */
		$uri = $this->getResolver()->getSslUri($request);
		return $this->toUrl($uri);
	}
	
	/**
	 * Return the URI Resolver Service
	 * @return UriResolver|NULL
	 */
	public function getResolver() {
		return $this->resolver;
	}
	
	/**
	 * Set the URI Resolver Service
	 * @param UriResolver
	 * @return ForceSsl
	 */
	public function setResolver(UriResolver $resolver) {
		$this->resolver = $resolver;
		return $this;
	}
	
	/**
	 * Whether redirect functionality is enabled as determined in config
	 * @return bool
	 */
	public function isEnabled() {
		return $this->getResolver()->getOptions()->isEnabled();
	}
	
}