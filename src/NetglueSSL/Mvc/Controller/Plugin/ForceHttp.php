<?php

namespace NetglueSSL\Mvc\Controller\Plugin;

class ForceHttp extends ForceSsl {
	
	/**
	 * If the current request is secured over SSL, redirect to the equivalent request for the standard http host configured
	 * @return void|Zend\Http\Response
	 */
	public function __invoke() {
		// Don't do anything if we're disabled...
		if(!$this->isEnabled()) {
			return;
		}
		$controller = $this->getController();
		$request = $controller->getRequest();
		
		/**
		 * If we're already connected with SSL, return
		 */
		if($this->getResolver()->isHttp($request)) {
			return;
		}
		
		/**
		 * Otherwise, redirect to the resolved HTTP Uri
		 */
		$uri = $this->getResolver()->getHttpUri($request);
		return $this->toUrl($uri);
	}
	
}