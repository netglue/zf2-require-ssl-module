<?php
namespace NetglueSSL;

/**
 * Autoloader
 */
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Loader\AutoloaderFactory;
use Zend\Loader\StandardAutoloader;

/**
 * Service Provider
 */
use Zend\ModuleManager\Feature\ServiceProviderInterface;

/**
 * Config Provider
 */
use Zend\ModuleManager\Feature\ConfigProviderInterface;

/**
 * Controller Plugin Provider
 */
use Zend\ModuleManager\Feature\ControllerPluginProviderInterface;

/**
 * Bootstrap Listener
 */
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\EventManager\EventInterface as Event;

class Module implements
	AutoloaderProviderInterface,
	ServiceProviderInterface,
	ConfigProviderInterface,
	BootstrapListenerInterface,
	ControllerPluginProviderInterface {
	
	
	/**
	 * Return autoloader configuration
	 * @link http://framework.zend.com/manual/2.0/en/user-guide/modules.html
	 * @return array
	 */
	public function getAutoloaderConfig() {
    return array(
			AutoloaderFactory::STANDARD_AUTOLOADER => array(
				StandardAutoloader::LOAD_NS => array(
					__NAMESPACE__ => __DIR__,
				),
			),
		);
	}
	
	/**
	 * Include/Return module configuration
	 * @return array
	 * @implements ConfigProviderInterface
	 */
	public function getConfig() {
		return include __DIR__ . '/../../config/module.config.php';
	}
	
	/**
	 * Return Service Config
	 * @return array
	 * @implements ServiceProviderInterface
	 */
	public function getServiceConfig() {
		return include __DIR__ . '/../../config/services.config.php';
	}
	
	/**
	 * Return controller plugin config
	 * @return array
	 */
	public function getControllerPluginConfig() {
		return array(
			'factories' => array(
				'NetglueSSL\Mvc\Controller\Plugin\ForceSsl' => function($sm) {
					$sl = $sm->getServiceLocator();
					$resolver = $sl->get('NetglueSSL\Service\UriResolver');
					$plugin = new Mvc\Controller\Plugin\ForceSsl;
					$plugin->setResolver($resolver);
					return $plugin;
				},
				'NetglueSSL\Mvc\Controller\Plugin\ForceHttp' => function($sm) {
					$sl = $sm->getServiceLocator();
					$resolver = $sl->get('NetglueSSL\Service\UriResolver');
					$plugin = new Mvc\Controller\Plugin\ForceHttp;
					$plugin->setResolver($resolver);
					return $plugin;
				},
			),
			'aliases' => array(
				'forceSSL' => 'NetglueSSL\Mvc\Controller\Plugin\ForceSsl',
				'forceHttp' => 'NetglueSSL\Mvc\Controller\Plugin\ForceHttp',
			),
		);
	}
	
	/**
	 * MVC Bootstrap Event
	 *
	 * @param Event $e
	 * @return void
	 * @implements BootstrapListenerInterface
	 */
	public function onBootstrap(Event $e) {
		$app = $e->getApplication();
		$serviceMgr = $app->getServiceManager();
		$options = $serviceMgr->get('NetglueSSL\Service\Options');
		if(!$options->isEnabled()) {
			// Skip the listener if the module is flagged disabled
			return;
		}
		/**
		 * Hook up to the MvcEvent::EVENT_ROUTE event after the Mvc\RouteListener so we can test for the EVENT_DISPATCH_ERROR
		 */
		$listener = new Mvc\RouteListener;
		$listener->setResolver($serviceMgr->get('NetglueSSL\Service\UriResolver'));
		
		$events = $app->getEventManager();
		$events->attachAggregate($listener);
	}
}
