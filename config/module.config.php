<?php
/**
 * Base Configuration for the Module
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 */

/**
 * Options for the module that are critical to its operation
 */
$moduleOptions = array(
	
	/**
	 * Whether to enable the module
	 */
	'enable' => true,
	
	/**
	 * If the SSL Hostname is not set, we assume that the SSL hostname and plain HTTP hostname are the same
	 */
	'ssl_hostname' => NULL,
	
	/**
	 * If the SSL and HTTP hostnames are different, we also need to know the http hostname
	 * Failure to set both will throw an exception
	 */
	'http_hostname' => NULL,
	
	/**
	 * If SSL runs on a non-standard port, we'll need to know
	 */
	'ssl_port' => NULL,
	
	/**
	 * And the same for HTTP
	 */
	'http_port' => NULL,
	
	/**
	 * When switching to SSL, we need to know if there is a prefix to the document root, for example https://www.example.com/secure/some-user/
	 * When empty, it is assumed that the document root is at /
	 */
	'ssl_path_prefix' => NULL,
	
	/**
	 * Getting back to plain http, if there is a path prefix such as http://www.example.com/my-app/ we need to know that too
	 * If empty, we assume document root is at /
	 */
	'http_path_prefix' => NULL,
	
	/**
	 * Configuration for forcing SSL by controller, route name or uri matching
	 */
	'ssl_only' => array(
		'controllers' => array(),
		'routes' => array(),
		'uris' => array(),
	),
	
	/**
	 * Configuration for forcing HTTP by controller, route name or uri matching
	 */
	'http_only' => array(
		'controllers' => array(),
		'routes' => array(),
		'uris' => array(),
	),
);

/**
 * Return config keyed with 'netglue_ssl'
 */
return array(
	'netglue_ssl' => array(
		'options' => $moduleOptions,
	),
);
